public class Pet {
    String name;
    String type;
    Cat cat = new Cat("", "", "");
    Dog dog = new Dog("", "", "");
    Cow cow = new Cow("", "", "");

    public static void main(String[] args) {

        voice();
    }

    static void voice() {
        System.out.println("I am " + Cat.type + ". My name is " + Cat.name + ". My voice is" + Cat.voice + ".");
        System.out.println("I am " + Dog.type + ". My name is " + Dog.name + ". My voice is" + Dog.voice + ".");
        System.out.println("I am " + Cow.type + ". My name is " + Cow.name + ". My voice is" + Cow.voice + ".");


    }
}

package Animal;

public interface Swimable {
    String swim();
}

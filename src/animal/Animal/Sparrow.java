package Animal;

public class Sparrow extends Animal implements Swimable, Flyable {
    public Sparrow(String name, String color) {
        this.name = name;
        this.color = color;
        this.type = Type.BIRD;
        this.countOfLegs = 2;
    }

    @Override
    void voice() {
        System.out.println("My name is " + this.name + "." +
                "My color is " + this.color + ". " +
                "I have " + this.countOfLegs + " legs " +
                "I have " + this.countOfWings + " wings" +
                swim() + fly() + " Who am i?");
    }

    @Override
    public String swim() {
        return "I can swim";
    }

    @Override
    public String fly() {
        return "I can Fly";
    }
}

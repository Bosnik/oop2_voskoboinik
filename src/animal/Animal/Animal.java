package Animal;

public abstract class Animal {
    protected String name;
    protected int countOfLegs;
    protected int countOfWings;
    protected String color;
    protected Type type;


    Lizard lizard;
    Salmon salmon;
    Sparrow sparrow;
    Cat cat;


    abstract void voice();


}

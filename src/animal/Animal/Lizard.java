package Animal;

class Lizard extends Animal implements Swimable, Runnable {




    public Lizard(String name, String color) {
            this.name = name;
            this.color = color;
            this.type = Type.AMPHIBIAN;
            this.countOfLegs = 4;
        }

    @Override
         void voice () {
            System.out.println("My name is " + this.name + "." +
                    "My color is " + this.color + ". " +
                    "I have " + this.countOfLegs + " legs " +
                    "I have " + this.countOfWings + " wings" +
                    run() + swim() + " Who am i?");
        }

        @Override
        public String run () {
            return " I can Run";
        }

        @Override
        public String swim () {
            return " I can swim";
        }

}

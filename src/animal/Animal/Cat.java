package Animal;

public class Cat extends Animal implements Runnable {
    public Cat(String name, String color) {
        this.name = name;
        this.color = color;
        this.type = Type.MAMMAL;
        this.countOfLegs = 4;
    }

    @Override
    void voice() {
        System.out.println("My name is " + this.name + "." +
                "My color is " + this.color + ". " +
                "I have " + this.countOfLegs + " legs " +
                "I have " + this.countOfWings + " wings" +
                run() + " Who am i?");
    }

    @Override
    public String run() {
        return "I can run";
    }
}

public class Dog extends Pet {
    static String name = "Djack";
    static String type = "Dog";
    static String voice = "Gav";

    public Dog(String name, String type, String voice) {
        this.name = name;
        this.type = type;
        this.voice = voice;
    }
}
